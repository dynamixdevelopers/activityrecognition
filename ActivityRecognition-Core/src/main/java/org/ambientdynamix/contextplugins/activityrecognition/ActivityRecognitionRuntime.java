/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.activityrecognition;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * Example auto-reactive plug-in that detects the device's battery level.
 *
 * @author shivam
 */
public class ActivityRecognitionRuntime extends ContextPluginRuntime {
    private final String TAG = this.getClass().getSimpleName();
    ArrayList<UUID> activityListeners;

    public ActivityRecognitionRuntime() {

    }

    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        this.setPowerScheme(powerScheme);
        Log.d(TAG, ActivityRecognitionRuntime.class.getName());
        this.activityListeners = new ArrayList<UUID>();
    }

    /**
     * Called by the Dynamix Context Manager to start (or prepare to start) context sensing or acting operations.
     */
    @Override
    public void start() {

    }

    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() {

        Log.d(TAG, "Stopped!");
    }

    /**
     * Stops the runtime (if necessary) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {
        this.stop();
        listener = null;
        activityListeners = null;
        Log.d(TAG, "Destroyed!");
    }

    IAndroidEventListener listener;

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {
        Log.d(TAG, "Received new context request of type : " + contextType);
        if (contextType.equalsIgnoreCase(ActivityInfo.CONTEXT_TYPE)) {
            if (listener == null) {
                listener = new IAndroidEventListener() {
                    @Override
                    public void onAndroidEvent(Intent intent) {
                        Log.d(TAG, "Received a new android event");
                        if (ActivityRecognitionResult.hasResult(intent)) {
                            List<DetectedActivity> result = ActivityRecognitionResult.extractResult(intent).getProbableActivities();
                            for (UUID uuid : activityListeners) {
                                sendContextEvent(uuid, new ActivityInfo(result));
                            }
                        }
                    }
                };
            }
            getPluginFacade().addAndroidEventListener(getSessionId(), IAndroidEventListener.ACTIVITY_RECOGNITION_EVENT, listener);
            sendContextRequestSuccess(requestId);
        } else {
            sendContextRequestError(requestId, "Context type not supported", ErrorCodes.NOT_SUPPORTED);
            Log.d(TAG, "Context type : " + contextType + " not supported");
        }
    }

    @Override
    public void handleConfiguredContextRequest(UUID requestId, String contextType, Bundle config) {
        // Warn that we don't handle configured requests
        Log.w(TAG, "handleConfiguredContextRequest called, but we don't support configuration!");
        // Drop the config and default to handleContextRequest
        handleContextRequest(requestId, contextType);
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // Not supported
    }

    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }

    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        Log.d(TAG, listenerInfo.toString());
        activityListeners.add(listenerInfo.getListenerId());
        return true;
    }

    @Override
    public boolean removeContextListener(UUID uuid) {
        activityListeners.remove(uuid);
        return true;
    }
}
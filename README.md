# Welcome
This README provides an overview of the `org.ambientdynamix.contextplugins.activityrecognition` plug-in for the [Dynamix Framework](http://ambientdynamix.org). It allows apps to recognize the physical activity the user is currently involved in.  

Note that this guide assumes an understanding of the [Dynamix Framework documentation](http://ambientdynamix.org/documentation/).

# Plug-in Requirements
* Dynamix version 2.1 and higher

# Context Support
* `org.ambientdynamix.contextplugins.activityrecognition.activityinfo` subscribe to current activity information data.

# Native App Usage
Integrate the Dynamix Framework within your app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your native programs, see [this guide](http://ambientdynamix.org/documentation/native-app-quickstart).

## Add context support for the plug-in.
```
#!java
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.activityrecognition", "org.ambientdynamix.contextplugins.activityrecognition.activityinfo", callback, listener);
```
## Handle Events
Once context support is successfully added for the `org.ambientdynamix.contextplugins.activityrecognition.activityinfo` type, the supplied listener will start receiving the activity change events. The events are of type `ActivityInfo`, which includes an `ArrayList` of `CustomDetectedActivity` objects. 

Each object of the `CustomDetectedActivity` type includes a type and a confidence value. Apps can make use of the `getConfidence` and `getType` methods of the class to retrieve the required values. 

## Supported Activities 

* IN_VEHICLE = 0;
* ON_BICYCLE = 1;
* ON_FOOT = 2;
* STILL = 3;
* UNKNOWN = 4;
* TILTING = 5;
* WALKING = 7;
* RUNNING = 8;








package org.ambientdynamix.contextplugins.activityrecognition;

import org.ambientdynamix.api.application.IContextInfo;

/**
 * Created by shivam on 8/17/15.
 */


public interface ICustomDetectedActivity extends IContextInfo {

    public int getType();

    public int getConfidence();

    public String getActivityString();
}

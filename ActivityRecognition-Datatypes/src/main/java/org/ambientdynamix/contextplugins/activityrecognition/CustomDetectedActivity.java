package org.ambientdynamix.contextplugins.activityrecognition;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by shivam on 8/17/15.
 */
public class CustomDetectedActivity implements ICustomDetectedActivity {
    /*
     * Type codes according to Google's DetectedActivity class.
     * https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity
     */
    public static final int IN_VEHICLE = 0;
    public static final int ON_BICYCLE = 1;
    public static final int ON_FOOT = 2;
    public static final int STILL = 3;
    public static final int UNKNOWN = 4;
    public static final int TILTING = 5;
    public static final int WALKING = 7;
    public static final int RUNNING = 8;
    public static Parcelable.Creator<CustomDetectedActivity> CREATOR = new Parcelable.Creator<CustomDetectedActivity>() {
        public CustomDetectedActivity createFromParcel(Parcel in) {
            return new CustomDetectedActivity(in);
        }

        public CustomDetectedActivity[] newArray(int size) {
            return new CustomDetectedActivity[size];
        }
    };
    int type;
    int confidence;

    private CustomDetectedActivity(Parcel in) {
        type = in.readInt();
        confidence = in.readInt();
    }

    public CustomDetectedActivity(int type, int confidence) {
        this.type = type;
        this.confidence = confidence;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(type);
        out.writeInt(confidence);
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public int getConfidence() {
        return confidence;
    }

    @Override
    public String getActivityString() {
        return getActivityType(type);
    }

    @Override
    public String getContextType() {
        return ActivityInfo.CONTEXT_TYPE;
    }

    @Override
    public String getImplementingClassname() {
        return CustomDetectedActivity.class.getSimpleName();
    }

    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("text/plain");
        return formats;
    }

    @Override
    public String getStringRepresentation(String s) {
        return "Activity " + getActivityType(type) + ", activity code " + type + ", confidence  " + confidence;
    }

    /**
     * Translates the type into into string-based versions of the activity, according to Google's documentation.
     * https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity
     *
     * @param type The type string to translate.
     * @return The activity as a string.
     */
    private String getActivityType(int type) {
        switch (type) {
            case IN_VEHICLE:
                return "IN_VEHICLE";
            case ON_BICYCLE:
                return "ON_BICYCLE";
            case ON_FOOT:
                return "ON_FOOT";
            case STILL:
                return "STILL";
            case TILTING:
                return "TILTING";
            case WALKING:
                return "WALKING";
            case RUNNING:
                return "RUNNING";
            case UNKNOWN:
                return "UNKNOWN";
        }
        return "NOT_FOUND";
    }
}
